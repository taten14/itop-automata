#!/usr/bin/python3
# -*- coding: utf-8 -*-
from itoptop import Itop
import re
import sys
import logging
import itoptop
import mysql.connector
from mysql.connector import Error
from mysql.connector.errors import custom_error_exception

def setITopConn():
    try:
        url='https://soporte.tcpcordoba.gov.ar/webservices/rest.php'
        ver = '1.3'
        usr = 'alertas'
        pwd='Am12HCPuypN4Vvpc6crE*'
        datamodel='/usr/lib/zabbix/alertscripts/datamodel-production.xml'
        return Itop(url, ver, usr, pwd, datamodel)
    except Exception as e:
        setLog(40,f'[setITopConn] {type(e).__name__}\n{e}')

def setLog(nivel, mensage):
    archivolog='/var/log/scripts/itop-automata.log'
    logging.basicConfig(level = logging.INFO, filename = archivolog, filemode = 'a+', format='%(asctime)s :: %(levelname)s :: %(message)s')
    logging.log(level=nivel, msg=mensage)

def setConn():
    try:
        connection = mysql.connector.connect(host='localhost',
                                            database='itop_automata_db',
                                            user='automata',
                                            password='O8WIosEaK8EDuoGbBms1')
        return connection
    except Exception as e:
        setLog(40,f'[setConn] {type(e).__name__}\n{e}')

def insertarRegistro(idZabbbixAction, idUserRequest):
    try:
        conn= setConn()
        cursor = conn.cursor()
        query=f'INSERT INTO rel_requests_zproblem (id_zproblem, id_iurequest, ongoing) VALUES ({idZabbbixAction}, {idUserRequest}, 1)'
        setLog(20,query)
        cursor.execute(query)
        conn.commit()
        setLog(20,f'[insertarRegistro] Registro insertado: {cursor.lastrowid}')
    except Exception as e:
        setLog(40,f'[insertarRegistro] {type(e).__name__}\n{e}')
    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()
            setLog(20,"[insertarRegistro] MySQL connection is closed")


def buscarRegistro(idZabbixAction):
    '''
    Recibe un idActionZabbix, devuelve True si encuentra el id en la tabla de relaciones
    '''
    try:
        conn = setConn()
        cursor = conn.cursor()
        query = f'SELECT id FROM rel_requests_zproblem rrz WHERE rrz.id_zproblem = {idZabbixAction};'
        setLog(20,query)
        cursor.execute(query)
        result = cursor.fetchall()
        setLog(20,f'Resultados obtenidos: {len(result)}')
        return True if len(result)>=1 else False
    except Exception as e:
        setLog(40,f'[buscarRegistro] {type(e).__name__}\n{e}')
    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()
            setLog(20,"[buscarRegistro] MySQL connection is closed")



def conectarDB(host, user, pwd, baseDatos):
    try:
        pass
    except Exception as e:
        setLog(40,f'{type(e).__name__}\n{e}')

def tagsToDict(tagString):
    '''
    Recibe: el body
    Devuelve un diccionario de los tags del body. 
    '''
    try:
        return {tag.split(':')[0]:tag.split(':')[1] for tag in re.search('TAGS\:(.*)',tagString).group(1).split(', ')}
    except Exception as e:
        setLog(40,f'[tagsToDict] {type(e).__name__}\n{e}')

def leerArchivos(filePath):
    try:
        with open(filePath,'r',encoding='utf-8') as filepointer:
            return f'{filepointer.read()}'
    except Exception as e:
        setLog(40,f'[filePath]{type(e).__name__}\n{e}')

def procesarAction(actionSubject, actionMessaje):
    try:
        subject_sections=re.search('^\[([0-9]*)\]\[(\w*)\](.*)',actionSubject)
        setLog(10,f'ID: {subject_sections.group(1)}, Tipo: {subject_sections.group(2)}, Subject: {subject_sections.group(3)}\nMensaje:\n{actionMessaje}')
        servicio = tagsToDict(actionMessaje)
        respuesta = {
            'action_id':f'{subject_sections.group(1)}',
            'tipo':f'{subject_sections.group(2)}',
            'subject':f'{subject_sections.group(3)}',
            #'mensaje':actionMessaje.split('\n'),
            'mensaje': ''''''.join([f'<p>{linea}</p>' for linea in actionMessaje.split('\n')]),
            'servicio': servicio
        }
        #if buscarRegistro(respuesta['action_id']):
        #    setLog(20,f'Registro {respuesta["action_id"]} encontrado')
        #else:
        #    setLog(20,f'Registro {respuesta["action_id"]} no encontrado')
        #print(servicio)
        #print(tagsToDict(actionMessaje))
        setLog(20,respuesta)
        return respuesta
    except Exception as e:
        setLog(40,f'[procesarAction]{type(e).__name__}\n{e}')

def buscarRelacion(actionID):
    try:
        pass
    except Exception as e:
        setLog(40,f'{type(e).__name__}\n{e}')

def crearRelacion(actionID,userRequestID):
    try:
        pass
    except Exception as e:
        setLog(40,f'{type(e).__name__}\n{e}')

def creaUserRequest(userRequestJSON):
    try:
        itop = setITopConn()
        userRequest = itop.UserRequest.insert(
            {
                'caller_id':'661',
                'org_id':userRequestJSON['servicio']['org_id'],
                'service_id':userRequestJSON['servicio']['servicio_id'],
                'servicesubcategory_id':userRequestJSON['servicio']['subservice_id'],
                'title':userRequestJSON['subject'],
                'description':f'{userRequestJSON["mensaje"]}'
            }
    )
        return userRequest
    except Exception as e:
        setLog(40,f'[creaUserRequest] {type(e).__name__}\n{e}')

def updateUserRequest(userRequestPublicLogJSON):
    try:
        pass
    except Exception as e:
        setLog(40,f'{type(e).__name__}\n{e}')

def cierraUserRequest(userRequestCerrarJSON):
    try:
        pass
    except Exception as e:
        setLog(40,f'{type(e).__name__}\n{e}')

def main(argv):
    setLog(20,f'{argv[1]} | {argv[2]}')
    userRequestJSON = procesarAction(argv[1],argv[2])
    if buscarRegistro(userRequestJSON['action_id']):
        setLog(20,f'Registro {userRequestJSON["action_id"]} encontrado')
    else:
        setLog(20,f'Registro {userRequestJSON["action_id"]} no encontrado')
        if userRequestJSON['tipo']=='CREACION':
            #setLog(40,f'[main] {userRequestJSON["tipo"]}')
            UserRequestResponse = creaUserRequest(userRequestJSON)
            setLog(20,f'{userRequestJSON["action_id"]}, {UserRequestResponse[0]["id"]}')
            insertarRegistro(userRequestJSON['action_id'], UserRequestResponse[0]['id'])
            setLog(20,f'Ref: {UserRequestResponse[0]["ref"]} ID: {UserRequestResponse[0]["id"]}')
            setLog(30,f'Ticket {UserRequestResponse[0]["ref"]} creado')

if __name__ == "__main__":
    main(sys.argv)