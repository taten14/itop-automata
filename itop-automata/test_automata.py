from itoptop import Itop
import re
import sys
import logging
import itoptop
import mysql.connector
from mysql.connector import Error

if __name__ == "__main__":
    url='https://soporte.tcpcordoba.gov.ar/webservices/rest.php'
    ver = '1.3'
    usr = 'alertas'
    pwd='Am12HCPuypN4Vvpc6crE*'
    datamodel='./datamodel-production.xml'
    itop = Itop(url, ver, usr, pwd, datamodel)
    #itop.UserRequest.find({'status': 'new'})
    #itop.UserRequest.find({'friendlyname': 'R-000088'})
    #itop.UserRequest.find({'friendlyname': 'R-000088'})
    #itop.UserRequest.find({'org_id_friendlyname': 'Operaciones','service_id_friendlyname': 'Reclamo Externos'})
    UserRequestResponse = itop.UserRequest.insert({
        'caller_id':'661',
        'org_id':'5',
        'service_id':'19',
        'servicesubcategory_id':'61',
        'title':'Prueba API',
        'description':'Prueba API'
    })
    itop.UserRequest.insert({
        'caller_id':'661',
        'org_id':'5',
        'service_id':'2',
        'servicesubcategory_id':'25',
        'title':'Prueba API',
        'description':'Prueba API'
    })


{
    'action_id': '1999563',
    'tipo': 'CREACION',
    'subject': '[Modernizacion Server] - Problem: El servcio pykms-server.service lleva caido mas de 1m',
    'mensaje': 'Problem started at 16:03:13 on 2021.12.03\r\nProblem name: El servcio pykms-server.service lleva caido mas de 1m\r\nSeverity: Warning\r\n\r\nOriginal problem ID: 1999563\r\n\r\n\r\nTAGS:cause:SVCDOWN, host:modernizacion, org_id:5, service:pykms-server.service, servicio_id:19, subservice_id:61, type:systemctl',
    'servicio': "{'cause': 'SVCDOWN', 'host': 'modernizacion', 'org_id': '5', 'service': 'pykms-server.service', 'servicio_id': '19', 'subservice_id': '61', 'type': 'systemctl'}"
    }


body='''Problem started at 14:10:29 on 2021.10.14
Problem name: Service "BITS3" (Servicio de transferencia inteligente en segundo plano (BITS)) is not running (startup type automatic delayed)
Host: UAT App Server
Severity: Information

Original problem ID: 1632300


TAGS:organization:operaciones, servicio:asasa, subservcio:carasa
'''
subject='[1632501][CREACION][Zabbix server] - Problem: Web scenario "SUAC DESA 00 TramitesARecibirPorUnidad" failed: 2'

subject_sections=re.search('^\[([0-9]*)\]\[(\w*)\](.*)',subject)

{tag.split(':')[0]:tag.split(':')[1] for tag in re.search('TAGS\:(.*)',body).group(1).split(', ')}

cuerpo='/var/log/scripts/automata_body.tx'
titulo='/var/log/scripts/automata_subject.tx'

procesarAction(leerArchivos(titulo),leerArchivos(cuerpo))

{ALERT.SUBJECT}

{ALERT.MESSAGE}


conn = setConn()
cursor = conn.cursor()
query = f'SELECT id FROM rel_requests_zproblem rrz WHERE rrz.id_zproblem = 654;'
cursor.execute(query)
result = cursor.fetchall()
cursor.close()
conn.shutdown()

prueba='''linea1
Linea2
Linea3
TAGS:action:ticket, cause:SVCDOWN, host:modernizacion, org_id:5, service:pykms-server.service, servicio_id:19, subservice_id:61, type:systemctl
'''

[f'<p>{linea}</p>' for linea in prueba[0:-1]]