#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import json
import sys
import logging
import ast

logging.basicConfig(
    level=logging.INFO,
    filename='/var/log/wh-slack/slack.log',
    filemode='a+',
    format='%(asctime)s :: %(levelname)s :: %(message)s')

def sanitizePayload(payload):
    try:
        jsonSanitized = ast.literal_eval(payload)
        cadenaLimpia = jsonSanitized['cards'][0]['sections'][0]['widgets'][-1]['textParagraph']['text']
        cadenaLimpia=cadenaLimpia.replace('"','')
        cadenaLimpia=cadenaLimpia.replace('{NL}','\n')
        cadenaLimpia=cadenaLimpia.replace('{COL}',':')
        jsonSanitized['cards'][0]['sections'][0]['widgets'][-1]['textParagraph']['text']=f'{cadenaLimpia}'
        logging.info(f'Cadena: {cadenaLimpia}')
        logging.info(f'JSON Sanitized: {jsonSanitized}')
        return jsonSanitized
    except Exception as e:
        logging.error(f'sanitizePayload: {e}')

def setRUL(urn):
    try:
        return f'https://hooks.slack.com{urn}'
    except Exception as e:
        logging.error(f'{e}')

def setMSG(jsonMSG):
    try:
        return json.dumps(jsonMSG)
    except Exception as e:
        logging.error(f'{e}')

def sendMSG(url, payload):
    try:
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        logging.info(response.text)
        logging.info(response.elapsed)
    except Exception as e:
        logging.error(f'{e}')

def main():
#    payload=sanitizePayload(sys.argv[2])
    payload=ast.literal_eval(sys.argv[2])
    logging.info(f'{sys.argv[1]}')
    logging.info(payload)
    sendMSG(
         setRUL(sys.argv[1]),
         json.dumps(payload)
   )

if __name__ == "__main__":
    main()
