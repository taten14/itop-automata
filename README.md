# Automatizador de creacion de Tickets

## Objetivo

- [x] Mediante un diccionario de servicios generar tickets de problemas informados por zabbix
- [x] La titpificacion debe ser dinamica, dependiendo de los tags asignados en zabbix
- [ ] Debe detectar si el ticket ya exsite en ITop en funcion del ID 

## Parametros


### Formato

El formato de salida de la app sera JSon

## Keys de UserRequests
```
{
    'operational_status':'',
    'ref':'',
	'org_id':'',
	'org_name':'',
	'caller_id':'',
	'caller_name':'',
	'team_id':'',
	'team_name':'',
	'agent_id':'',
	'agent_name':'',
	'title':'',
	'description':'',
	'start_date':'',
	'end_date':'',
	'last_update':'',
	'close_date':'',
	'private_log':'',
	'contacts_list':'',
	'functionalcis_list':'',
	'workorders_list':'',
	'status':'',
	'request_type':'',
	'impact':'',
	'priority':'',
	'urgency':'',
	'origin':'',
	'service_id':'',
	'service_name':'',
	'servicesubcategory_id':'',
	'servicesubcategory_name':'',
	'escalation_flag':'',
	'escalation_reason':'',
	'assignment_date':'',
	'resolution_date':'',
	'last_pending_date':'',
	'cumulatedpending':'',
	'tto':'',
	'ttr':'',
	'tto_escalation_deadline':'',
	'sla_tto_passed':'',
	'sla_tto_over':'',
	'ttr_escalation_deadline':'',
	'sla_ttr_passed':'',
	'sla_ttr_over':'',
	'time_spent':'',
	'resolution_code':'',
	'solution':'',
	'pending_reason':'',
	'parent_request_id':'',
	'parent_request_ref':'',
	'parent_problem_id':'',
	'parent_problem_ref':'',
	'parent_change_id':'',
	'parent_change_ref':'',
	'related_request_list':'',
	'public_log':'',
	'user_satisfaction':'',
	'user_comment':'',
	'service_details':'',
	'finalclass':'',
	'friendlyname':'',
	'org_id_friendlyname':'',
	'org_id_obsolescence_flag':'',
	'caller_id_friendlyname':'',
	'caller_id_obsolescence_flag':'',
	'team_id_friendlyname':'',
	'team_id_obsolescence_flag':'',
	'agent_id_friendlyname':'',
	'agent_id_obsolescence_flag':'',
	'service_id_friendlyname':'',
	'servicesubcategory_id_friendlyname':'',
	'parent_request_id_friendlyname':'',
	'parent_problem_id_friendlyname':'',
	'parent_change_id_friendlyname':'',
	'id':''
}
```

## Mensaje de ALerta

### Subject

TBD

### Body

TBD

### Tipificacion

TBD

### Adjuntos

TBD

## Actions

Los actions se disparan cuando un Trigger se activa.
Para que la app cree el ticket en el Action tenemos que tener el **Default subject** definido asi **[{EVENT.ID}][CREACION][{HOST.NAME}] - Problem: {EVENT.NAME}**. EN este nombre la aplicacion identificara 3 campos.
- **1 [EVENT.ID]**: Campo en el que dispondra del ID de la incidencia Zabbix que genero el ticket. Servira mas adelante para identificar un ticket en la base de datos
- **2 [ACCION]**: Accion que se llevara a cabo con el ID. Puede ser CREACION, UPDATE, o CLOSE
- **3 [HOST.NAME]**: Campo que trae el Hostname desde Zabbix.

```
[{HOST.NAME}] - Problem: {EVENT.NAME}

Problem started at {EVENT.TIME} on {EVENT.DATE}
Problem name: {EVENT.NAME}
Host: {HOST.NAME}
Severity: {EVENT.SEVERITY}

Original problem ID: {EVENT.ID}
{TRIGGER.URL}
```

## Tags Requeridos

|TAG|Valor|Nota|Estado|
|---|---|---|---|
|action|ticket|Genera un ticket en ITOP|Produccion|
|on_close|CLOSE / UPDATE|este Tag se usa para definir que se debe hacer cuando en el body se detecta un CLOSE. Puede adoptar el valor update, o close. Si el TAG no se encuentra se asume UPDATE|Desarrollo|
|org_id||Equivalente a la query OQL **SELECT Organization**|Produccion
|servicio_id||Equivalente a la query OQL **SELECT Service**|Produccion
|subservice_id||Equivalente a la query OQL **SELECT ServiceSubcategory**|Produccion

## Tags requerido para filtrar Actions

Estos valores deben estar para poder realizar Actions personalizadas

|TAG|Valor|Nota|Estado|
|---|---|---|---|
|type||Tipo de incidenica. Se indica con mayusculas y sin espacios.|Produccion|
|subtype||SubtipoSe indica con mayusculas y sin espacios.|Produccion|
|env||Se usa en el caso de que se deba indicar en que entorno aplica la alerta|Produccion|
|cause||Se puede usar como un tercer nivel de  type / subtype.Se indica con mayusculas y sin espacios.|Produccion|

## TAGs Dinamicos de Zabbix

win_ev_session_init_type

```
{{ITEM.LASTVALUE}.regsub("\t+Tipo\sde\sinicio\sde\ssesión\:\t+(.*)\n",\1)}
```

win_ev_ip_address

```
{{ITEM.VALUE}.regsub("\t+Dirección\sde\sred\sde\sorigen\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("\t+Dirección\sde\sred\sde\sorigen\:\t+(.*)\n",\1)}
```

win_ev_workstation

```
{{ITEM.VALUE}.regsub("\t+Nombre\sde\sestación\sde\strabajo\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("\t+Nombre\sde\sestación\sde\strabajo\:\t+(.*)\n",\1)}
```

win_ev_domain

```
{{ITEM.VALUE}.regsub("\t+Dominio\sde\scuenta\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("\t+Dominio\sde\scuenta\:\t+(.*)\n",\1)}
```

win_ev_session_id

```
{{ITEM.VALUE}.regsub("\t+Id\.\sde\sinicio\sde\ssesión\:\t+(.*)\n",\1)}
{{ITEM.VALUE}.regsub("Nuevo\sinicio\sde\ssesión\:\n.*\n.*\n.*\n.*\n.*\t+(.*)\n",\1)}

{{ITEM.LASTVALUE}.regsub("\t+Id\.\sde\sinicio\sde\ssesión\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("Nuevo\sinicio\sde\ssesión\:\n.*\n.*\n.*\n.*\n.*\t+(.*)\n",\1)}
```

win_ev_vinculated_session

```
{{ITEM.VALUE}.regsub("\t+Inicio\sde\ssesión\svinculado\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("\t+Inicio\sde\ssesión\svinculado\:\t+(.*)\n",\1)}

{{ITEM.LASTVALUE}.regsub("\t+Inicio\sde\ssesión\svinculado\:\t+(.*)\n",\1)}
```

win_ev_username

```
{{ITEM.LASTVALUE}.regsub("\t+Nombre\sde\scuenta\:\t+(.*)\n",\1)}
{{ITEM.LASTVALUE}.regsub("Nuevo\sinicio\sde\ssesión\:\n\t+.*\n\t+.*\t+(.*)\n",\1)}
```

win_event_userame

```
(?<=Nuevo\sinicio\sde\ssesión:\n)(?sx).*
```
## Actions Definidas

Ticket DNS
 - Value of tag action equals ticket
 - Value of tag type contains DNS

Ticket Servicio Internet
 - Value of tag type contains INET_SVC
 - Value of tag action equals ticket

Ticket Servicios Linux
 - Value of tag action equals ticket
 - Value of tag cause contains SVCDOWN
 - Value of tag type contains systemctl

Tickets de SSL
- SSL Vencido o por vencer

## Problemas



|Familia de Servicios ITop|Id|
|---|---|
|Operaciones|1|
|Innovacion|2|
|Desarrollo|3|



### DNS

#### Reclamos de DNS de Gobierno

|Key|Value|
| --- | --- |
|action|ticket|
|org_id|5|
|servicio_id|16|
|subservice_id|58|
|dns_record|MX\|A|

#### Reclamos de DNS de Don Web

|Key|Value|
| --- | --- |
|action|ticket|
|org_id|5|
|servicio_id|10|
|subservice_id|32|
|dns_record|MX\|A|

### Servicio de Internet

#### Reclamo Servicio Internet

|Key|Value|
| --- | --- |
|action|ticket|
|org_id|5|
|servicio_id|10|
|subservice_id|33|
|type|INET_SVC|
|svcname|FIBERCORP\|IPLAN|

### Dominios

#### Reclamo Dominio por Vencer

|Key|Value|
| --- | --- |
|action|ticket|
|org_id|5|
|servicio_id|19|
|subservice_id|63|
|type|NIC_DOMAIN|
|svcname|NIC_AR|
|env|PROD|
|cause|DOMAIN_EXPIRED|

### Sistemas Operativos
#### Reclamo Servicio Linux Caido

Cuando cae algun servicio Linux monitorizado

|Key|Value|
| --- | --- |
|action|ticket|
|org_id|5|
|servicio_id|19|
|subservice_id|61|
|type|systemctl|
|action|ticket|

### Tribunal Digital


## Proveedores y Servicios

Quienes proveen que servicio desde la optica de ITop

TBD
